Source: dbskkd-cdb
Section: utils
Priority: optional
Maintainer: Tatsuya Kinoshita <tats@debian.org>
Build-Depends: debhelper-compat (= 13), libcdb-dev
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/debian/dbskkd-cdb
Vcs-Git: https://salsa.debian.org/debian/dbskkd-cdb.git
Homepage: https://github.com/jj1bdx/dbskkd-cdb
Rules-Requires-Root: no

Package: dbskkd-cdb
Architecture: any
Provides: skkserv
Conflicts: skkserv
Replaces: skkserv
Depends: ${shlibs:Depends}, ${misc:Depends}, openbsd-inetd | inet-superserver, skkdic-cdb
Recommends: tinycdb
Enhances: skk
Description: SKK dictionary server using cdb for faster access
 dbskkd-cdb is a dictionary server for SKK Japanese input systems,
 using D. J. Bernstein's cdb database for faster dictionary access.
 .
 dbskkd-cdb is compatible with skkserv on the protocol behavior.
 It is called from an internet super-server.
 .
 This package uses the SKK dictionary cdb file `SKK-JISYO.cdb' that
 is provided by the skkdic-cdb package.
 .
 If you want to convert your own dictionary to cdb, use the
 `makeskkcdbdic' command.  This command requires the tinycdb package.
